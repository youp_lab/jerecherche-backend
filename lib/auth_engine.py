import json

from django.contrib import messages
from django.contrib.auth import get_user_model

from lib.resources import confs
from lib.config import django_logger, headers
from lib.middleware import exec_api_call

api = confs.get('api')


class LDAPAuthenticationBackend:
    """
    Authenticate against the OCI LDAP Auth.
    """
    user_model = get_user_model()

    @staticmethod
    def ad_auth(credentials):
        data = {}
        user = {}
        ad = api["AD"]
        url = ad['url']
        method = ad['method']

        params = {
            "username": credentials["username"],
            "password": credentials["password"]
        }

        data["headers"] = headers
        data["params"] = json.dumps(params)

        ad_call = exec_api_call(url, method, data, scope = "AD")

        if ad_call:
            auth = ad_call
            status = auth["check"]
            if status == "true":
                try:
                    user["check"] = status
                    user["username"] = credentials["username"]
                    user["nom"] = auth["nom"]
                    user["prenom"] = auth["prenom"]
                    user["matricule"] = auth["matricule"]
                    user["email"] = auth["email"]
                    user["commonName"] = auth["commonName"]
                    user["fonction"] = auth["fonction"]
                    user["department"] = auth["department"]
                except KeyError as e:
                    return django_logger.error("Undefined key  => {}".format(e))
            api_status = True
        else:
            api_status = False
            django_logger.error("AD API NOT AVAILABLE !!! Return => {}".format(ad_call))
        if not bool(user):
            user = None

        return user, api_status

    def authenticate(self, request, username = None, password = None):
        credentials = {"username": username, "password": password}

        logged_in = self.ad_auth(credentials = credentials)

        user, auth = logged_in

        if (bool(user) or user is not None) and auth:
            return self.get_or_create_user(request, credentials, user)
        elif auth and not user:
            messages.info(request, "Identifiants non trouvés !")
            django_logger.info(request, "Identifiants non trouvés !")
            return None
        else:
            messages.error(request, "Un problème est survenu, merci de reessayer ultérieurement SVP !")
            django_logger.error(request, "Un problème est survenu, merci de reessayer ultérieurement SVP !")
            return None

    def get_or_create_user(self, request, credentials, auth):

        username = credentials["username"]
        u = {}

        kwargs = {
            'username': credentials["username"],
            'reg_number': auth.get("matricule", ""),
            'email': auth.get("email", ""),
            'last_name': auth.get("nom", ""),
            'first_name': auth.get("prenom", ""),
            'function': auth.get("fonction", ""),
            'department': auth.get("department", "")
        }

        try:
            u = self.user_model.objects.get(username__exact = username)
        except self.user_model.DoesNotExist as e:
            django_logger.info(request, "{}".format(e))
            u = self.user_model.objects.create(**kwargs)
        finally:
            return self.get_user(u.id)

    def get_user(self, user_id):
        try:
            return self.user_model.objects.get(pk = user_id)
        except self.user_model.DoesNotExist:
            return None
