import logging
import threading

from django_backend.settings import cfg

APP_NAME = cfg.get('PROJECT', 'APP_NAME')

django_logger = logging.getLogger('django')

headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}

_thread_locals = threading.local()


def set_current_user(user):
    _thread_locals.user = user


def get_current_user():
    return getattr(_thread_locals, 'user', None)
