import decimal
import itertools
import json
import os
import random
import re
import string
from ast import literal_eval
from math import cos, asin, sqrt

import requests
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.db.models.fields.files import ImageFieldFile
from django.utils.deprecation import MiddlewareMixin
from django.utils.text import slugify
from requests.adapters import HTTPAdapter
from urllib3 import Retry

from lib.config import django_logger, set_current_user, get_current_user
from lib.resources import confs

app_name = __package__.split('.')[0]
notification = confs.get('notification')


class Hider(object):

    def __get__(self, instance, owner):
        raise AttributeError('Hidden attribute')

    def __set__(self, obj, val):
        raise AttributeError('Hidden attribute')


class CurrentUserMiddleware(MiddlewareMixin):
    def process_request(self, request):
        set_current_user(getattr(request, 'user', None))


class Monitor(models.Model):
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.SET_NULL, blank = True, null = True,
                                   related_name = 'created_%(class)ss', editable = False)
    updated_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.SET_NULL, blank = True, null = True,
                                   related_name = 'updated_%(class)ss', editable = False)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        user = get_current_user()
        if user and user.is_authenticated:
            self.updated_by = user
            if not self.id:
                self.created_by = user
        super().save(*args, **kwargs)


def upload_path(instance, filename):
    ext = filename.split('.')[-1]
    model = "{}".format(instance.__class__.__name__.lower())
    name = "{0}_{1}".format(model, generate_id()) if not hasattr(instance, 'slug') else instance.slug
    file = '{0}.{1}'.format(name, ext)
    path = os.path.join("uploads", model)
    return os.path.join(path, file)


def generate_string(size, return_type = 'password|pin'):
    chars = string.digits
    if return_type == 'password':
        chars += string.ascii_lowercase
    return ''.join(random.choice(chars) for _ in range(size))


def generate_id():
    import uuid
    return uuid.uuid4()


def requests_retry_session(
        retries = 1,
        backoff_factor = 0.3,
        status_forcelist = (500, 502, 504),
        session = None,
):
    session = session or requests.Session()
    retry = Retry(
        total = retries,
        read = retries,
        connect = retries,
        backoff_factor = backoff_factor,
        status_forcelist = status_forcelist,
    )
    adapter = HTTPAdapter(max_retries = retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)

    return session


def api_call(url = "", method = "", data = None, timeout = 15, retries = 1, verify = False, auth = None):
    try:
        if data is None:
            data = {}

        if "headers" in data:
            headers = data["headers"]
        else:
            headers = {}

        if "params" in data:
            params = data["params"]
        else:
            params = {}

        django_logger.info("{0} => {1}".format(method, url))

        r = None

        if method:
            if method == 'GET':
                r = requests_retry_session(retries = retries).get(url, headers = headers, params = params, timeout = timeout, verify = verify,
                                                                  auth = auth)
            elif method == 'PUT':
                r = requests_retry_session(retries = retries).put(url, headers = headers, data = params, timeout = timeout, verify = verify,
                                                                  auth = auth)
            elif method == 'POST':
                r = requests_retry_session(retries = retries).post(url, headers = headers, data = params, timeout = timeout, verify = verify,
                                                                   auth = auth)
            elif method == 'DELETE':
                r = requests_retry_session(retries = retries).delete(url, headers = headers, timeout = timeout)

            django_logger.info("{}".format(r))

        return r
    except requests.exceptions.ConnectionError as e:
        django_logger.error("Network problem occurred => {}".format(e))
        return {"status": False, "message": "{}".format(e)}
    except requests.exceptions.HTTPError as e:
        django_logger.error("Invalid HTTP response => {}".format(e))
        return {"status": False, "message": "{}".format(e)}
    except requests.exceptions.Timeout as e:
        django_logger.error("Oops ! The request has timed out => {}".format(e))
        return {"status": False, "message": "{}".format(e)}
    except requests.exceptions.RequestException as e:
        django_logger.error("An error occured => {}".format(e))
        return {"status": False, "message": "{}".format(e)}


def exec_api_call(url = "", method = "", params = None, scope = ''):
    data = {}
    data["headers"] = params.get('headers')
    if method != "GET":
        data["params"] = params.get('params')
    call = api_call(url, method, data)
    if "status" in call:
        return {}
    if not url_works(call.status_code):
        django_logger.error("{} API => {} : {}".format(scope, call.status_code, call.reason))
        return None
    return call.json()


def url_works(status_code):
    if 200 <= status_code < 400:
        return True
    else:
        return False


def py_slugify(s):
    # Remove all non-word characters (everything except numbers and letters)
    s = re.sub(r"[^\w\s]", '', s)

    # Replace all runs of whitespace with a single underscore
    s = re.sub(r"\s+", '_', s)

    return s


def dict_to_querydict(dictionary):
    from django.http import QueryDict
    from django.utils.datastructures import MultiValueDict

    qdict = QueryDict('', mutable = True)

    if type(dictionary) is dict:
        for key, value in dictionary.items():
            d = {key: value}
            qdict.update(MultiValueDict(d) if isinstance(value, list) else d)

    elif type(dictionary) is QueryDict:
        qdict.update(MultiValueDict(dictionary))

    return qdict


class JSONEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if hasattr(obj, 'isoformat'):  # handles both date and datetime objects
            return obj.isoformat()
        elif isinstance(obj, decimal.Decimal):
            return float(obj)
        elif isinstance(obj, ImageFieldFile):
            try:
                return obj.url
            except ValueError as e:
                return ''
        else:
            return json.JSONEncoder.default(self, obj)


def get_unique_slug(instance, title):
    slug_candidate = slug_original = slugify(title)
    for i in itertools.count(1):
        try:
            exists = instance.__class__._default_manager.get(slug = slug_candidate)
            if instance.pk != exists.pk:
                slug_candidate = '{}-{}'.format(slug_original, i)
            else:
                break
        except ObjectDoesNotExist:
            break

    return slug_candidate


def get_fields_and_properties(model, instance, fields = None, has_monitor = True):
    if fields is not None:
        field_names = fields
    else:
        field_names = [f.name for f in model._meta.fields]

    if not has_monitor:
        field_names = [f.name for f in model._meta.fields if f.name not in [f.name for f in Monitor._meta.get_fields()]]

    property_names = [name for name in dir(model) if isinstance(getattr(model, name), property) if name not in ['pk', 'id']]
    return dict((name, "{}".format(getattr(instance, name))) for name in field_names + property_names)


def str_to_json(str = ""):
    if len(str) > 1:
        return literal_eval(str)
    else:
        return {}


def distance(lat1, lon1, lat2, lon2):
    p = 0.017453292519943295
    a = 0.5 - cos((lat2 - lat1) * p) / 2 + cos(lat1 * p) * cos(lat2 * p) * (1 - cos((lon2 - lon1) * p)) / 2
    return 12742 * asin(sqrt(a))


def closest(data, v):
    return sorted(data, key = lambda p: distance(v['lat'], v['lon'], p['lat'], p['lon']))


def get_class_instance(klass, pk = None, slug = None):
    try:
        if slug is not None:
            return klass.objects.get(slug = slug)
        return klass.objects.get(pk = pk)
    except ObjectDoesNotExist as e:
        return None


def get_place_list_by_coords(klass, geo_loc, coords):
    if bool(geo_loc) and bool(coords):
        place_list = [{'pk': x['pk'], 'lat': float(x['lat']) if x['lat'] else 0, 'lon': float(x['lon']) if x['lon'] else 0} for x in coords]
        closest_places = closest(place_list, geo_loc)

        return [get_class_instance(klass, x['pk']) for x in closest_places]
    return coords
