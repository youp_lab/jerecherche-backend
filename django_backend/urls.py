"""django_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, re_path
from django.utils.translation import ugettext_lazy

from django_backend.settings import cfg

admin.site.site_title = ugettext_lazy('{}'.format(cfg.get('PROJECT', 'APP_DESCRIPTION')))
admin.site.site_header = ugettext_lazy('{}'.format(cfg.get('PROJECT', 'APP_DESCRIPTION')))

urlpatterns = [
    path('', include('apps.api.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('api/', include('apps.api.urls')),
    # path('api/oneci/', include('apps.oneci.urls')),
    # path('api/wizall/', include('apps.wizall.urls')),
    path('search/', include('apps.frontend.urls')),
    path('search/oneci/', include('apps.oneci.urls')),
    path('search/wizall/', include('apps.wizall.urls')),
    path('admin/', admin.site.urls),

]

# Add Django site authentication urls (for login, logout, password management)
urlpatterns += [
    path('accounts/', include('django.contrib.auth.urls')),
]

# Add 'prefix' to all urlpatterns
# urlpatterns = [re_path(r'^{}/'.format(settings.APP_URL_ENDPOINT), include(urlpatterns))]

if settings.DEBUG is True:
    import debug_toolbar

    urlpatterns = [path('__debug__/', include(debug_toolbar.urls)), ] + urlpatterns
    urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)

handler500 = 'rest_framework.exceptions.server_error'
handler400 = 'rest_framework.exceptions.bad_request'
