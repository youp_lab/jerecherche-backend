import locale
import os
import sys
from configparser import RawConfigParser
from pathlib import Path

from django.contrib import messages
from django.utils.translation import gettext_lazy as _

app_name = __package__.split('.')[0]
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)

BASE_DIR = Path(__file__).resolve(strict = True).parent.parent
CONF_DIR = os.path.join(BASE_DIR, 'conf')
RESOURCES_DIR = BASE_DIR.joinpath('lib/resources')
LOG_DIR = BASE_DIR.joinpath('logs')

APPS_DIR = os.path.realpath(BASE_DIR.joinpath('apps'))
sys.path.append(APPS_DIR)

cfg = RawConfigParser()
cfg.read(os.path.join(BASE_DIR, 'conf') + '/settings.conf')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = cfg.get('SETTINGS', 'SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = cfg["SETTINGS"].getboolean("DEBUG")

ALLOWED_HOSTS = cfg.get('SETTINGS', 'ALLOWED_HOSTS').split(',')

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'sorl.thumbnail',
    'apps.api.apps.ApiConfig',
    'apps.location.apps.LocationConfig',
    'apps.oneci.apps.OneciConfig',
    'apps.wizall.apps.WizallConfig',
    'apps.frontend.apps.FrontendConfig',
    'crispy_forms',
]

CRISPY_TEMPLATE_PACK = 'bootstrap4'
CRISPY_FAIL_SILENTLY = not DEBUG

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'lib.middleware.CurrentUserMiddleware',
]

ROOT_URLCONF = 'django_backend.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'django_backend.wsgi.application'

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': cfg.get('DB', 'DB_NAME'),
        'USER': cfg.get('DB', 'DB_USER'),
        'PASSWORD': cfg.get('DB', 'DB_PASSWORD'),
        'HOST': cfg.get('DB', 'DB_HOST'),
        'PORT': cfg.get('DB', 'DB_PORT'),
    }
}

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTH_USER_MODEL = 'api.UserProfile'

AUTHENTICATION_BACKENDS = (
    # 'lib.auth_engine.LDAPAuthenticationBackend',
    'django.contrib.auth.backends.ModelBackend',
)

CORS_ORIGIN_ALLOW_ALL = True

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = cfg.get('SETTINGS', 'LANGUAGE_CODE')

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/
STATIC_URL = '/static/'
STATIC_ROOT = cfg.get('PROJECT', 'STATIC_ROOT')

MEDIA_URL = '/media/'
MEDIA_ROOT = cfg.get('PROJECT', 'MEDIA_ROOT')

# Other configs
APPEND_SLASH = True

APP_URL_ENDPOINT = cfg.get('PROJECT', 'APP_URL').split("/")[-1]
LOGIN_URL = '/accounts/login'

# LOGIN_REDIRECT_URL = '/'

LANGUAGES = [
    ('fr', _('French')),
]

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = cfg.get('SETTINGS', 'USE_TZ')

locale.setlocale(locale.LC_ALL, cfg.get('SETTINGS', 'LOCALE'))

MESSAGE_LEVEL = messages.DEBUG

MESSAGE_TAGS = {
    messages.DEBUG: 'info',
    messages.INFO: 'info',
    messages.SUCCESS: 'success',
    messages.WARNING: 'warning',
    messages.ERROR: 'error',
}

TIME_INPUT_FORMATS = [
    '%H:%M',  # '14:30'
]

DATE_INPUT_FORMATS = ['%d %B %Y', ]

if DEBUG:
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

from .confs import *
