from django_backend.settings import cfg

EMAIL_HOST = cfg.get('EMAIL', 'EMAIL_HOST')
EMAIL_PORT = cfg.getint('EMAIL', 'EMAIL_PORT')
EMAIL_HOST_USER = cfg.get('EMAIL', 'EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = cfg.get('EMAIL', 'EMAIL_HOST_PASSWORD')
EMAIL_USE_TLS = cfg.get('EMAIL', 'EMAIL_USE_TLS')
EMAIL_USE_SSL = cfg.get('EMAIL', 'EMAIL_USE_SSL')
EMAIL_TIMEOUT = cfg.getint('EMAIL', 'EMAIL_TIMEOUT')
