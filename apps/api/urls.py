from django.urls import path
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()

urlpatterns = [
    path('', views.is_alive, name = "is-alive"),
]

urlpatterns += router.urls
