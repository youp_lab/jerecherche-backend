from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from lib.middleware import upload_path


class UserProfile(AbstractUser):
    password = models.CharField(_('password'), max_length = 128, null = True, editable = False)
    picture = models.ImageField(upload_to = upload_path, blank = True, null = True)

    class Meta:
        db_table = "user"

    def __str__(self):
        return "{} {}".format(self.last_name, self.first_name)

    @property
    def full_name(self):
        return "{} {}".format(self.last_name, self.first_name)
