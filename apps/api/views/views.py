from datetime import datetime, timezone

from django.http import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny


@api_view(['GET'])
@permission_classes([AllowAny])
def is_alive(request):
    alive_date = datetime.now(tz = timezone.utc)
    return JsonResponse({'is_alive': 'yes', 'date': alive_date})


def custom404(request):
    return JsonResponse({
        'status': False,
        'context': "RESOURCE_NOT_FOUND"
    }, status = status.HTTP_404_NOT_FOUND)


def custom500(request):
    return JsonResponse({
        'status': False,
        'context': "INTERNAL_SERVER_ERROR"
    }, status = status.HTTP_500_INTERNAL_SERVER_ERROR)
