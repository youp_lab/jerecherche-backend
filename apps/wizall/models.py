from datetime import datetime

from django.db import models
from sorl.thumbnail import ImageField

from apps.location.models import Location, Geo
from lib.middleware import Monitor, upload_path


class SalePointType(Monitor):
    name = models.CharField(max_length = 60, verbose_name = 'Nom')
    description = models.TextField(default = '', blank = True, verbose_name = 'Description')

    def __str__(self):
        return "{}".format(self.description)


class SalePoint(Monitor, Location, Geo):
    type = models.ManyToManyField(SalePointType, related_name = 'sale_point_types', verbose_name = 'Type')
    name = models.CharField(max_length = 120)
    slug = models.SlugField(unique = True, editable = False, blank = True, null = True)
    description = models.TextField(default = '', blank = True, verbose_name = 'Description')
    contact = models.CharField(max_length = 45, blank = True, verbose_name = 'Contact Téléphonique')
    fax = models.CharField(max_length = 45, blank = True, verbose_name = 'Fax')
    website = models.URLField(blank = True, null = True, verbose_name = 'Site Web')
    image = ImageField(upload_to = upload_path, blank = True, null = True, verbose_name = 'Image')
    particularity = models.CharField(max_length = 150, default = '', blank = True, verbose_name = 'Disposition Particulière')
    opening_time = models.TimeField(blank = True, null = True, verbose_name = 'Heure d\'ouverture')
    closing_time = models.TimeField(blank = True, null = True, verbose_name = 'Heure de fermeture')

    class Meta:
        ordering = ['name']

    def __str__(self):
        return "{}".format(self.name)

    @property
    def is_open(self):
        if None not in (self.opening_time, self.closing_time):
            return self.opening_time <= datetime.now().time() < self.closing_time
        return True
