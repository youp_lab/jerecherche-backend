from django.apps import AppConfig


class WizallConfig(AppConfig):
    name = 'apps.wizall'
