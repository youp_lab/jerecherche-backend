from django.apps import apps
from django.contrib import admin

# Register your models here.
from apps.wizall.models import SalePoint, SalePointType

for model in apps.get_app_config('wizall').models.values():
    admin.site.register(model)


class SalePointTypeAdmin(admin.ModelAdmin):
    search_fields = ['name']


class SalePointAdmin(admin.ModelAdmin):
    list_filter = ('type',)
    autocomplete_fields = ['type', 'city', 'town', 'borough']
    list_display = ('name', 'city', 'town', 'borough', 'opening_time', 'closing_time',)


admin.site.unregister(SalePointType)
admin.site.register(SalePointType, SalePointTypeAdmin)
admin.site.unregister(SalePoint)
admin.site.register(SalePoint, SalePointAdmin)
