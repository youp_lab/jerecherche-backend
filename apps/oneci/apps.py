from django.apps import AppConfig


class OneciConfig(AppConfig):
    name = 'apps.oneci'
