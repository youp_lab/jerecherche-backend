# Create your views here.
from rest_framework import viewsets, permissions

from apps.oneci.serializers import *


class PlaceTypeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows place types to be viewed or edited.
    """
    queryset = PlaceType.objects.all().order_by('name')
    serializer_class = PlaceTypeSerializer
    permission_classes = [permissions.IsAuthenticated]


class PlaceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows place types to be viewed or edited.
    """
    queryset = Place.objects.all().order_by('name')
    serializer_class = PlaceSerializer
    permission_classes = [permissions.IsAuthenticated]
