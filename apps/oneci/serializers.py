from rest_framework import serializers

from apps.oneci.models import *


class PlaceTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlaceType
        fields = ('pk', 'name', 'description')


class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Place
        fields = (
            'pk', 'type', 'name', 'contact', 'fax', 'website', 'particularity', 'opening_time', 'closing_time', 'city', 'town', 'borough',
            'location_detail', 'lat', 'lon'
        )
