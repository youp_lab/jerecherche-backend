from datetime import datetime

from django.db import models
from sorl.thumbnail import ImageField

from apps.location.models import Location, Geo
from lib.middleware import Monitor, upload_path


class PlaceType(Monitor):
    name = models.CharField(max_length = 60, verbose_name = 'Nom')
    description = models.TextField(default = '', blank = True, verbose_name = 'Description')

    def __str__(self):
        return "{}".format(self.description)


class Place(Monitor, Location, Geo):
    type = models.ForeignKey(PlaceType, models.RESTRICT, verbose_name = 'Type')
    name = models.CharField(max_length = 120)
    slug = models.SlugField(unique = True, editable = False, blank = True, null = True)
    description = models.TextField(default = '', blank = True, verbose_name = 'Description')
    contact = models.CharField(max_length = 45, blank = True, verbose_name = 'Contact Téléphonique')
    fax = models.CharField(max_length = 45, blank = True, verbose_name = 'Fax')
    website = models.URLField(blank = True, null = True, verbose_name = 'Site Web')
    image = ImageField(upload_to = upload_path, blank = True, null = True, verbose_name = 'Image')
    particularity = models.CharField(max_length = 150, default = '', blank = True, verbose_name = 'Disposition Particulière')
    opening_time = models.TimeField(blank = True, null = True, verbose_name = 'Heure d\'ouverture')
    closing_time = models.TimeField(blank = True, null = True, verbose_name = 'Heure de fermeture')

    class Meta:
        ordering = ['name']

    def __str__(self):
        return "{}".format(self.name)

    @property
    def is_open(self):
        if None not in (self.opening_time, self.closing_time):
            return self.opening_time <= datetime.now().time() < self.closing_time
        return True


class RequestType(Monitor):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 150, verbose_name = 'Nom')
    image = ImageField(upload_to = upload_path, blank = True, null = True, verbose_name = 'Image')

    class Meta:
        abstract = True


class Request(Monitor):
    id = models.BigAutoField(primary_key = True)
    type = models.ForeignKey(RequestType, models.SET_DEFAULT, default = 1, verbose_name = 'Type')
    reference = models.CharField(max_length = 25, blank = True, editable = False)
    place = models.ForeignKey(Place, models.PROTECT, verbose_name = 'Lieu de prière')
    sender_last_name = models.CharField(max_length = 160, verbose_name = 'Nom')
    sender_first_name = models.CharField(max_length = 160, verbose_name = 'Prénom(s)')
    sender_contact = models.CharField(max_length = 45, verbose_name = 'Contact Téléphonique')
    sender_email = models.EmailField(max_length = 160, verbose_name = 'Email')
    description = models.TextField(verbose_name = 'Demande')
    start_date = models.DateField(verbose_name = 'Date de début')
    end_date = models.DateField(verbose_name = 'Date de fin')

    class Meta:
        abstract = True
