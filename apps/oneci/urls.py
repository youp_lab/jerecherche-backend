from django.urls import path, include
# from rest_framework.routers import DefaultRouter

from apps.frontend.views.oneci import *

# router = DefaultRouter()
app_name = 'oneci'

# router.register(r'place-type', views.PlaceTypeViewSet)
# router.register(r'place', views.PlaceViewSet)

# urlpatterns = [
# path('', include(router.urls)),
# ]

# urlpatterns += router.urls


urlpatterns = [
    path('', PlaceListView.as_view(), name = "place-list"),
    path('place-search', PlaceSearchView.as_view(), name = "place-search"),
    path('<int:pk>', PlaceListView.as_view(), name = "place-detail"),
]
