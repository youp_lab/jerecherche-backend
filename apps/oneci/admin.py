from django.apps import apps
from django.contrib import admin

# Register your models here.
from apps.oneci.models import Place, PlaceType

for model in apps.get_app_config('oneci').models.values():
    admin.site.register(model)


class PlaceTypeAdmin(admin.ModelAdmin):
    search_fields = ['name']


class PlaceAdmin(admin.ModelAdmin):
    list_filter = ('type',)
    autocomplete_fields = ['type', 'city', 'town', 'borough']
    list_display = ('type', 'name', 'city', 'town', 'borough', 'opening_time', 'closing_time',)


admin.site.unregister(PlaceType)
admin.site.register(PlaceType, PlaceTypeAdmin)
admin.site.unregister(Place)
admin.site.register(Place, PlaceAdmin)
