from django.contrib import admin

# Register your models here.
from apps.location.models import *


class CityAdmin(admin.ModelAdmin):
    search_fields = ['name']


class TownAdmin(admin.ModelAdmin):
    autocomplete_fields = ['city']
    search_fields = ['name']


class BoroughAdmin(admin.ModelAdmin):
    autocomplete_fields = ['town']
    search_fields = ['name']


admin.site.register(City, CityAdmin)
admin.site.register(Town, TownAdmin)
admin.site.register(Borough, BoroughAdmin)
