from django.urls import path, re_path

from .views import *

app_name = 'frontend'

# AJAX PATTERNS
urlpatterns = [
    re_path(r'^ajax/location-search$', location_search, name = 'location-search'),
    re_path(r'^ajax/place-search$', place_search, name = 'get-place'),
    re_path(r'^ajax/get-location$', get_location, name = 'get-location'),
    re_path(r'^ajax/place-filter/(?P<module>\w+)$', place_filter, name = "place-filter"),
]
