import json

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView


class IndexView(LoginRequiredMixin, TemplateView):
    template_name = 'frontend/index.html'

    # paginate_by = cfg.get('SETTINGS', 'PAGINATE_BY')

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'place'
        context['page_name'] = 'place_list'
        context['page_title'] = 'Liste des centres'
        data = [get_fields_and_properties(Place, place, has_monitor = False) for place in self.object_list]
        place_result = json.dumps(data, cls = JSONEncoder)
        context['place_result'] = place_result
        context['search_form'] = PlaceSearchForm()

        return context
