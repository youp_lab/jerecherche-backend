import json
from datetime import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.views.generic import ListView

from apps.frontend.forms import ONECIPlaceSearchForm
from apps.oneci.models import Place
# from django_backend.settings import cfg
from lib.middleware import JSONEncoder, get_fields_and_properties, str_to_json, get_place_list_by_coords


class PlaceListView(LoginRequiredMixin, ListView):
    model = Place
    context_object_name = 'place_list'
    template_name = 'frontend/search/oneci.html'

    # paginate_by = cfg.get('SETTINGS', 'PAGINATE_BY')

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['module'] = 'oneci'
        context['menu'] = 'place'
        context['page_name'] = 'place_list'
        context['page_title'] = 'Liste des centres'
        data = [get_fields_and_properties(Place, place, has_monitor = False) for place in self.object_list]
        place_result = json.dumps(data, cls = JSONEncoder)
        context['place_result'] = place_result
        context['search_form'] = ONECIPlaceSearchForm()

        return context


class PlaceSearchView(LoginRequiredMixin, ListView):
    def post(self, request, *args, **kwargs):
        form = ONECIPlaceSearchForm(data = request.POST)
        post = form.data

        place_name = post.get('name', '')
        place_type = post.get('type', '')
        place_location = post.get('location', '')

        search_result = Place.objects.all()
        if len(place_type) > 0:
            search_result = search_result.filter(type = place_type)
        if len(place_name) > 0:
            search_result = search_result.filter(name__icontains = place_name)
        if len(place_location) > 0:
            search_result = search_result.filter(
                Q(city__name__icontains = place_location) |
                Q(town__name__icontains = place_location) |
                Q(borough__name__icontains = place_location)
            )

        data = [get_fields_and_properties(Place, place, has_monitor = False) for place in search_result]
        place_result = json.dumps(data, cls = JSONEncoder)

        context = {
            'module': 'oneci',
            'place_result': place_result,
            'place_list': search_result,
            'place_list_count': search_result.count(),
        }

        template = render_to_string('frontend/layouts/partials/place-search.html', context)

        data = {
            'template': template,
        }

        return JsonResponse(data, safe = False)


class PlaceFilterView(LoginRequiredMixin, ListView):
    model = Place

    def post(self, request, *args, **kwargs):
        post = request.POST

        geo_loc = str_to_json(post.get('geo_loc', ''))
        data = str_to_json(post.get('data', ''))
        place_filter = post.get('filter', '')

        search_result, count_search_result = None, 0

        place_result = {}
        from_geo_loc = False

        if place_filter == 'all':
            search_result = self.model.objects.all()
            count_search_result = search_result.count()
        if place_filter == 'distance':
            search_result = get_place_list_by_coords(self.model, geo_loc, data)
            count_search_result = len(search_result)
            from_geo_loc = True
        if place_filter == 'opened':
            data = [place['pk'] for place in data]
            current_time = datetime.now().time()
            search_result = self.model.objects.filter(pk__in = data) \
                .filter(Q(closing_time__isnull = True) | (Q(opening_time__lte = current_time) & Q(closing_time__gte = current_time)))

            count_search_result = search_result.count()

        if search_result:
            data = [get_fields_and_properties(Place, place, has_monitor = False) for place in search_result]
            place_result = json.dumps(data, cls = JSONEncoder)

        context = {
            'module': 'oneci',
            'from_geo_loc': from_geo_loc,
            'place_result': place_result,
            'place_list': search_result,
            'place_list_count': count_search_result,
        }

        template = render_to_string('frontend/layouts/partials/place-search.html', context)

        data = {
            'template': template,
        }

        return JsonResponse(data, safe = False)
