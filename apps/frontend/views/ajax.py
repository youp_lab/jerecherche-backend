import json
from datetime import datetime
from itertools import chain

from django.apps import apps
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt

from apps.location.models import *
from apps.oneci.models import Place
from apps.wizall.models import SalePoint
from lib.config import django_logger
from lib.middleware import api_call, str_to_json, get_place_list_by_coords, get_fields_and_properties, JSONEncoder
from lib.resources import confs


@csrf_exempt
def place_search(request):
    post = request.POST
    place_name = post.get('place_name', '')
    place_type = post.get('place_type', '')

    status = False
    results = []

    place = Place.objects.all()
    if len(place_type) > 0:
        place = place.filter(type = place_type)
    if len(place_name) > 0:
        place = place.filter(name__icontains = place_name)

    if place:
        status = True
        results = list(place.values())

    data = {
        'status': status,
        'results': results,
    }

    return JsonResponse(data, safe = False)


@csrf_exempt
def location_search(request):
    post = request.POST
    location = post.get('location', None)

    status = False
    results = []

    cities_list = City.objects.filter(name__icontains = location).values()
    city = [dict(x, type = 'city') for x in cities_list]

    towns_list = Town.objects.filter(name__icontains = location).values()
    town = [dict(x, type = 'town') for x in towns_list]

    boroughs_list = Borough.objects.filter(name__icontains = location).values()
    borough = [dict(x, type = 'borough') for x in boroughs_list]

    values = sorted(chain(city, town, borough), key = lambda instance: instance['name'])

    if values:
        status = True
        results = values

    data = {
        'status': status,
        'results': results,
    }

    return JsonResponse(data, safe = False)


@csrf_exempt
def get_location(request):
    latitude = request.POST.get('latitude', None)
    longitude = request.POST.get('longitude', None)

    if latitude and longitude:
        geo_api = confs.get('api')['GEO_API']

        url = geo_api['url'].replace('{latitude}', latitude).replace('{longitude}', longitude)
        place = api_call(url = url, method = geo_api['method'], timeout = 5)

        if not hasattr(place, 'status_code'):
            out_msg = place["message"]
            django_logger.error("GEO API NOT REACHABLE !!! => {}".format(out_msg))

            return JsonResponse(None, safe = False)
        else:
            r = place.text
            return JsonResponse(json.loads(r))


@login_required
def place_filter(request, module):
    post = request.POST

    model = Place if module == 'oneci' else SalePoint
    # model = apps.get_model(module, model)

    geo_loc = str_to_json(post.get('geo_loc', ''))
    data = str_to_json(post.get('data', ''))
    place_filter = post.get('filter', '')

    search_result, count_search_result = None, 0

    place_result = {}
    from_geo_loc = False

    if place_filter == 'all':
        search_result = model.objects.all()
        count_search_result = search_result.count()
    if place_filter == 'distance':
        search_result = get_place_list_by_coords(model, geo_loc, data)
        count_search_result = len(search_result)
        from_geo_loc = True
    if place_filter == 'opened':
        data = [place['pk'] for place in data]
        current_time = datetime.now().time()
        search_result = model.objects.filter(pk__in = data) \
            .filter(Q(closing_time__isnull = True) | (Q(opening_time__lte = current_time) & Q(closing_time__gte = current_time)))

        count_search_result = search_result.count()

    if search_result:
        data = [get_fields_and_properties(Place, place, has_monitor = False) for place in search_result]
        place_result = json.dumps(data, cls = JSONEncoder)

    context = {
        'module': module,
        'from_geo_loc': from_geo_loc,
        'place_result': place_result,
        'place_list': search_result,
        'place_list_count': count_search_result,
    }

    template = render_to_string('frontend/layouts/partials/place-search.html', context)

    data = {
        'template': template,
    }

    return JsonResponse(data, safe = False)
