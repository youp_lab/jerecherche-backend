const place_search_form = $('form#place-search-form')
const place_search_list = $('.place-search-list')
const place_results = $('.place-result')
let coords = null

$(document).on("click", ".place-search-submit", function (e) {
    e.preventDefault()
    $('a.search_mob').click()
    place_search_list.addClass('spinner')
    hideAllMarkers()
    place_results.attr('data-place-result', '')

    $.post("place-search", place_search_form.serialize())
        .done(function (response) {
            setTimeout(function () {
                place_search_list.removeClass('spinner')
                place_search_list.empty()
                place_search_list.html(response.template)
                let places = $('.place-result').attr('data-place-result')
                showAllMarkers(JSON.parse(places))
            }, 1000)
        })
        .fail(function () {
            alert("Nous avons rencontré une erreur, merci de reessayer ultérieurement")
        })
        .always(function () {
            place_search_list.show("slow")
        })
})
