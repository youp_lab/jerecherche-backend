/* PLACE SEARCH */

// Autocomplete

const placeOptions = {
    url: function (place_name, place_type) {
        return "/search/ajax/place-search";
    },

    getValue: function (element) {
        return element.name;
    },

    ajaxSettings: {
        dataType: "json",
        method: "POST",
        data: {
            dataType: "json"
        }
    },

    preparePostData: function (data) {
        data.place_name = $("#id_name").val()
        data.place_type = $("#id_type").val()
        return data;
    },

    listLocation: "results",

    list: {
        showAnimation: {
            type: "slide"
        },
        hideAnimation: {
            type: "slide"
        }
    },

    requestDelay: 400
};

$("#id_name").easyAutocomplete(placeOptions);
