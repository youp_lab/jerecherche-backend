/* LOCATION SEARCH */

// Autocomplete

const locationOptions = {
    url: function (location) {
        return "/search/ajax/location-search";
    },

    getValue: function (element) {
        return element.name;
    },

    ajaxSettings: {
        dataType: "json",
        method: "POST",
        data: {
            dataType: "json"
        }
    },

    preparePostData: function (data) {
        data.location = $("#id_location").val()
        return data;
    },

    listLocation: "results",

    list: {
        showAnimation: {
            type: "slide"
        },
        hideAnimation: {
            type: "slide"
        }
    },

    requestDelay: 400
};

$("#id_location").easyAutocomplete(locationOptions);

// let geo_loc = $("input[name='geo_loc']")
// geo_loc.val(JSON.stringify({coords: coords, address: address}))

const distanceOptions = {units: 'kilometers'}

function calcDistance(lat, lon) {
    if (coords !== null) {
        const from = turf.point([coords.lat, coords.lon]);
        const to = turf.point([lat, lon]);
        return turf.distance(from, to, distanceOptions);
    }
}

function getDistance() {
    const place_distance = $('.from_geo_loc .distance')
    place_distance.each(function () {
        let lat = $(this).attr('data-lat');
        let lon = $(this).attr('data-lon');
        if (lat.length > 0 && lon.length > 0) {
            let distance = calcDistance(lat, lon);
            $(this).text(distance.toFixed(2) + " KM");
            $(this).attr('data-distance', distance);
        } else {
            $(this).closest(".from_geo_loc").html("(à venir)");
            $(this).attr('data-distance', 0);
        }
    })
}

function extractData(data) {
    let objectArray = []
    let dataObj = JSON.parse(data);

    $.each(dataObj, function (key, value) {
        objectArray.push(
            {
                pk: parseInt(value.id),
                lat: parseFloat(value.lat),
                lon: parseFloat(value.lon),
            }
        )
    })
    return objectArray
}

function renderAll(filter, module) {
    let csrfmiddlewaretoken = $("form#place-filters").find("input[name='csrfmiddlewaretoken']").val()
    let place_list = $('.place-result').attr('data-place-result')
    const postData = {
        csrfmiddlewaretoken,
        filter,
        data: JSON.stringify(extractData(place_list))
    }

    $.post("/search/ajax/place-filter/" + module, postData)
        .done(function (response) {
            if (response !== null) {
                setTimeout(function () {
                    place_search_list.removeClass('spinner')
                    place_search_list.empty()
                    place_search_list.html(response.template)
                }, 1000)
            } else {
                alert('Aucun résultat trouvé')
            }
        })
        .fail(function () {
            alert("Nous avons rencontré une erreur, merci de reessayer ultérieurement")
        })
}

function renderByDistance(position, filter, module) {
    let csrfmiddlewaretoken = $("form#place-filters").find("input[name='csrfmiddlewaretoken']").val()
    let geo_loc = JSON.stringify(position)
    let place_list = $('.place-result').attr('data-place-result')
    const postData = {
        csrfmiddlewaretoken,
        geo_loc,
        filter,
        data: JSON.stringify(extractData(place_list))
    }

    $.post("/search/ajax/place-filter/" + module, postData)
        .done(function (response) {
            if (response !== null) {
                setTimeout(function () {
                    place_search_list.removeClass('spinner')
                    place_search_list.empty()
                    place_search_list.html(response.template)
                    getDistance(position)
                }, 1000)
            } else {
                alert('Aucun résultat trouvé')
            }
        })
        .fail(function () {
            alert("Nous avons rencontré une erreur, merci de reessayer ultérieurement")
        })
}

function renderByTime(filter, module) {
    let csrfmiddlewaretoken = $("form#place-filters").find("input[name='csrfmiddlewaretoken']").val()
    let place_list = $('.place-result').attr('data-place-result')
    const postData = {
        csrfmiddlewaretoken,
        filter,
        data: JSON.stringify(extractData(place_list))
    }

    $.post("/search/ajax/place-filter/" + module, postData)
        .done(function (response) {
            if (response !== null) {
                setTimeout(function () {
                    place_search_list.removeClass('spinner')
                    place_search_list.empty()
                    place_search_list.html(response.template)
                }, 1000)
            } else {
                alert('Aucun résultat trouvé')
            }
        })
        .fail(function () {
            alert("Nous avons rencontré une erreur, merci de reessayer ultérieurement")
        })
}

$("form#place-filters input").click(function () {
    place_search_list.addClass('spinner')
})

$('input#distance').click(function (e) {
    let filter = $(this).val()
    let module = $(this).attr('data-module')
    if (coords === null) {
        if ("geolocation" in navigator) {
            const options = {
                enableHighAccuracy: true,
                timeout: 10000,
                maximumAge: 0
            }

            function success(position) {
                coords = {"lat": position.coords.latitude, "lon": position.coords.longitude}
                renderByDistance(coords, filter, module)
            }

            function error(err) {
                console.warn("ERREUR " + err.code + ": " + err.message);
                alert("ERREUR : Nous n'avons pas pu vous localiser !!!");
            }

            navigator.geolocation.getCurrentPosition(success, error, options)
        } else {
            alert("Le service de localisation n'est pas disponible.")
        }
    } else {
        renderByDistance(coords, filter, module)
    }
})

$('input#opened').click(function (e) {
    let filter = $(this).val()
    let module = $(this).attr('data-module')
    renderByTime(filter, module)
})

$('input#all').click(function (e) {
    let filter = $(this).val()
    let module = $(this).attr('data-module')
    renderAll(filter, module)
})