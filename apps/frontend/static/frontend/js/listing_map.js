(function (A) {
    if (!Array.prototype.forEach)
        A.forEach = A.forEach || function (action, that) {
            for (let i = 0, l = this.length; i < l; i++)
                if (i in this)
                    action.call(that, this[i], i, this)
        }
})(Array.prototype)

let mapObject, markers, mgr, initialLocation

function Places(place_list) {
    let data = []
    $.each(place_list, function (key, value) {
        data.push({
            pk: parseInt(value.id),
            type_point: value.type ?? '',
            name: value.name,
            location_latitude: parseFloat(value.lat),
            location_longitude: parseFloat(value.lon),
            map_image_url: '/static/frontend/img/thumb_map_single_hotel.jpg',
            rate: 'Superb | 7.5',
            is_opened: value.is_open === 'True' ? 'Ouvert' : 'Fermé',
            name_point: value.name,
            get_directions_start_address: '',
            phone: value.contact.length > 0 ? value.contact : "à venir",
            url_point: 'detail-hotel.html'
        })
    })
    return data
}

function Markers(place_list) {
    place_list.forEach(function (item) {
        let marker = new google.maps.Marker({
            pk: item.pk,
            position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
            map: mapObject,
            icon: '/static/frontend/img/pins/' + 'Marker' + '.png',
        })

        if ('undefined' === typeof markers)
            markers = []
        markers.push(marker)
        marker.addListener('click', function () {
            closeInfoBox()
            getInfoBox(item).open(mapObject, this)
            mapObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude))
        })
    })
}

if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (position) {
        initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
        coords = {"lat": position.coords.latitude, "lon": position.coords.longitude}
        mapObject.setCenter(initialLocation)
    }, function (positionError) {
        // User denied geolocation prompt - default to Abidjan
        mapObject.setCenter(new google.maps.LatLng(5.316667, -4.033333))
        mapObject.setZoom(11)
    })
}

const mapOptions = {
    zoom: 13,
    //center: new google.maps.LatLng(48.865633, 2.321236),
    mapTypeId: google.maps.MapTypeId.ROADMAP,

    mapTypeControl: false,
    mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        position: google.maps.ControlPosition.LEFT_CENTER
    },
    panControl: false,
    panControlOptions: {
        position: google.maps.ControlPosition.TOP_RIGHT
    },
    zoomControl: true,
    zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_BOTTOM
    },
    scrollwheel: false,
    scaleControl: false,
    scaleControlOptions: {
        position: google.maps.ControlPosition.TOP_LEFT
    },
    streetViewControl: true,
    streetViewControlOptions: {
        position: google.maps.ControlPosition.LEFT_TOP
    },
    styles: [
        {
            "featureType": "administrative.country",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.province",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.locality",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.neighborhood",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "landscape.man_made",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "landscape.natural.landcover",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "landscape.natural.terrain",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.business",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.government",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.medical",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.place_of_worship",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.school",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.sports_complex",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.highway.controlled_access",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "transit.line",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit.station.airport",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit.station.bus",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit.station.rail",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        }
    ]
}

function initMap() {
    mapObject = new google.maps.Map(document.getElementById('map_right_listing'), mapOptions)

    let place_list = Places(places)
    Markers(place_list)

    // Add a marker clusterer to manage the markers.
    mgr = new MarkerClusterer(mapObject, markers)
}

function hideAllMarkers() {
    markers.forEach(function (marker) {
        marker.setMap(null)
        marker = null
    })
    markers = []
    mgr.clearMarkers()
}

function showAllMarkers(places) {
    let place_list = Places(places)
    Markers(place_list)
    // markers.forEach(function (marker) {
    //     marker.setMap(mapObject)
    // })
    mgr.addMarkers(markers)
    //google.maps.event.trigger(mapObject, 'resize')
}

function closeInfoBox() {
    $('div.infoBox').remove()
}

function getInfoBox(item) {
    return new InfoBox({
        content:
            '<div class="marker_info" id="marker_info">' +
            '<img src="' + item.map_image_url + '" alt=""/>' +
            '<span>' +
            '<span class="infobox_rate">' + item.is_opened + '</span>' +
            '<em>' + item.type_point + '</em>' +
            '<h3>' + item.name_point + '</h3>' +
            '<strong>' + item.description_point + '</strong>' +
            '<a href="' + item.url_point + '" class="btn_infobox_detail"></a>' +
            '<form action="http://maps.google.com/maps" method="get" target="_blank">' +
            '<input name="saddr" value="' + item.get_directions_start_address + '" type="hidden">' +
            '<input type="hidden" name="daddr" value="' + item.location_latitude + ',' + item.location_longitude + '">' +
            '<button type="submit" value="Get directions" class="btn_infobox_get_directions">Je m\'y rends</button>' +
            '</form>' +
            '<a href="tel://' + item.phone + '" class="btn_infobox_phone">' + item.phone + '</a>' +
            '</span>' +
            '</div>',
        disableAutoPan: false,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(10, 92),
        closeBoxMargin: '',
        closeBoxURL: "/static/frontend/img/close_infobox.png",
        isHidden: false,
        alignBottom: true,
        pane: 'floatPane',
        enableEventPropagation: true
    })
}

function onHtmlClick(key) {
    const obj = markers.find((o) => {
        return o['pk'] === key
    })
    google.maps.event.trigger(obj, "click")
    //google.maps.event.trigger(markers[location_type][key], "click")
}

let places = place_results.data('place-result')
initMap()
