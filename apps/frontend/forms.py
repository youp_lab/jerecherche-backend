from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Submit, Div, Field
from django import forms
from django.utils.translation import gettext as _

from apps.location.models import City, Town, Borough
from apps.oneci.models import *
from apps.wizall.models import *


class ONECIPlaceSearchForm(forms.Form):
    type = forms.ModelChoiceField(
        label = 'Type', required = False, widget = forms.Select(attrs = {'class': 'wide'}), queryset = PlaceType.objects.all(), empty_label = _('Tout')
    )
    name = forms.CharField(
        label = 'Nom', required = False,
        widget = forms.TextInput(attrs = {'class': 'form-control place', 'placeholder': 'Saisissez votre recherche', 'autocomplete': 'off'}),
        max_length = 150
    )
    city = forms.ModelChoiceField(
        label = 'Ville', required = False, queryset = City.objects.all(), empty_label = _('Indifférent')
    )
    town = forms.ModelChoiceField(
        label = 'Commune', required = False, queryset = Town.objects.all(), empty_label = _('Indifférent')
    )
    borough = forms.ModelChoiceField(
        label = 'Ville', required = False, queryset = Borough.objects.all(), empty_label = _('Indifférent')
    )
    location = forms.CharField(
        label = 'Ville', required = False, widget = forms.TextInput(attrs = {'class': 'form-control', 'placeholder': 'Où ?'}), max_length = 150
    )


class WIZALLPlaceSearchForm(forms.Form):
    type = forms.ModelChoiceField(
        label = 'Type', required = False, widget = forms.Select(attrs = {'class': 'wide'}), queryset = SalePointType.objects.all(), empty_label = _('Tout')
    )
    name = forms.CharField(
        label = 'Nom', required = False,
        widget = forms.TextInput(attrs = {'class': 'form-control place', 'placeholder': 'Saisissez votre recherche', 'autocomplete': 'off'}),
        max_length = 150
    )
    city = forms.ModelChoiceField(
        label = 'Ville', required = False, queryset = City.objects.all(), empty_label = _('Indifférent')
    )
    town = forms.ModelChoiceField(
        label = 'Commune', required = False, queryset = Town.objects.all(), empty_label = _('Indifférent')
    )
    borough = forms.ModelChoiceField(
        label = 'Ville', required = False, queryset = Borough.objects.all(), empty_label = _('Indifférent')
    )
    location = forms.CharField(
        label = 'Ville', required = False, widget = forms.TextInput(attrs = {'class': 'form-control', 'placeholder': 'Où ?'}), max_length = 150
    )
