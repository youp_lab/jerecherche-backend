from backend.functions import omc_api_call
from backoffice import BASE_PATH, omc, headers


def get_corporate_list():
    url = "{0}{1}".format(BASE_PATH, omc["CORPORATES"]["url"])
    method = "GET"

    corporate_list = omc_api_call(url, method)

    if corporate_list is not None:
        return corporate_list
    else:
        return False


def get_corporate_details(corporate_id = None):
    url = "{0}{1}".format(BASE_PATH, omc["CORPORATE_DETAILS"]["url"])
    method = "GET"
    url = url.replace('{corporate_id}', "{}".format(corporate_id))

    corporate = omc_api_call(url, method)

    if corporate is not None:
        return corporate
    else:
        return False


def create_corporate(data):
    url = "{0}{1}".format(BASE_PATH, omc["CORPORATES"]["url"])
    method = "POST"
    
    corporate = omc_api_call(url, method, params = data)

    if corporate is not None:
        return corporate
    else:
        return False


def update_corporate(data, corporate_id = None):
    url = "{0}{1}".format(BASE_PATH, omc["CORPORATE_DETAILS"]["url"])
    method = "PUT"
    url = url.replace('{corporate_id}', "{}".format(corporate_id))

    corporate = omc_api_call(url, method, params = data)

    if corporate is not None:
        return corporate
    else:
        return False
